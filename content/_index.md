+++
title = "Introduction"
+++

This website's mostly there to host my [blog](@/blog/_index.md). That is where I 
share my knowledge, opinions and frustrations about programming, self-hosting, 
(largely music-related) electronics and embedded systems, and {% lsc() %}DIY{% 
end %} projects of all kinds. Don't expect any sort of regularity or thematic 
uniformity.

## About me

I work as a (programming) consultant in the finance industry. I used to work as 
an embedded developer.

I'm a versatile programmer proficient in C++, Python, C, C#, Bash, Javascript, 
etc.
Enthusiastic about {% lsc() %}TDD,{% end %} {% lsc() %}CI/CD,{% end %} DevOps, 
infrastructure-as-code, code generation, static analysis and parsers/compilers.
Proficient Ansible user, long-time Linux user, <span class="lsc">FOSS</span> 
enthusiast and contributor. LaTeX (and typography in general) enthusiast.

I have an eclectic music taste. I also play guitar (poorly and just as a 
hobby), tinker with audio effects and electronic appliances in general, host my 
own services. I love flea markets and scraping classifieds.

I live in Ljubljana, Slovenia.

## Links

- [Gitlab](https://gitlab.com/andrejr): where I host my personal projects
- [Github](https://github.com/randrej): just for contributing to other people's 
  projects
- [PyPI](https://pypi.org/user/andrejr/): my Python packages
- [AUR](https://aur.archlinux.org/account/andrejr): packages I maintain on Arch 
  Linux's user repository
- [CTAN](https://pypi.org/user/andrejr/): my LaTeX packages
- [LinkedIn](https://www.linkedin.com/in/andrej-radovi%C4%87-bb0ab2209/): not 
  very active
- [RYM](https://rateyourmusic.com/~andrejr): my music ratings
- [last.fm](https://www.last.fm/user/shumar): what I listen to

## Contact

You can contact me at <cold-email@andrejradovic.com>.
