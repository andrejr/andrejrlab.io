#!/bin/bash

# Define the input and output file names
INPUT_FILE="favicon.svg"
OUTPUT_FOLDER="static"
OPTIMIZED_SVG_FILE="$OUTPUT_FOLDER/favicon.svg"
ICO_FILE="$OUTPUT_FOLDER/favicon.ico"
TOUCH_ICON_FILE="$OUTPUT_FOLDER/apple-touch-icon.png"
MANIFEST_FILE="$OUTPUT_FOLDER/manifest.webmanifest"
ICON_SIZES=(192 512)

# Create the output folder if it doesn't exist
mkdir -p $OUTPUT_FOLDER

# Create temporary files for the PNG files
ICON_32="/tmp/icon-32.png"
ICON_24="/tmp/icon-24.png"
ICON_16="/tmp/icon-16.png"

# Create the PNG files using rsvg-convert
rsvg-convert -w 32 -h 32 -o $ICON_32 $INPUT_FILE
rsvg-convert -w 24 -h 24 -o $ICON_24 $INPUT_FILE
rsvg-convert -w 16 -h 16 -o $ICON_16 $INPUT_FILE

# Create the ICO file using ImageMagick
convert $ICON_16 $ICON_24 $ICON_32 $ICO_FILE

# Remove the temporary PNG files
rm $ICON_32 $ICON_24 $ICON_16

# Create the apple touch icon using rsvg-convert
rsvg-convert -w 180 -h 180 -o $TOUCH_ICON_FILE $INPUT_FILE

# Create the icon files for the manifest using rsvg-convert
for size in ${ICON_SIZES[@]}; do
    rsvg-convert -w ${size} -h ${size} -o "$OUTPUT_FOLDER/icon-${size}.png" $INPUT_FILE
done

# Optimize the svg favicon
svgo "$INPUT_FILE" -o "$OPTIMIZED_SVG_FILE"

# Create the manifest file
# cat <<EOF >$MANIFEST_FILE
# {
#    "name": "Your Web App Name",
#    "short_name": "Short Name",
#    "icons": [
#        {
#            "src": "icon-${ICON_SIZES[0]}.png",
#            "sizes": "${ICON_SIZES[0]}x${ICON_SIZES[0]}",
#            "type": "image/png"
#        },
#        {
#            "src": "icon-${ICON_SIZES[1]}.png",
#            "sizes": "${ICON_SIZES[1]}x${ICON_SIZES[1]}",
#            "type": "image/png"
#        }
#    ],
#    "theme_color": "#ffffff",
#    "background_color": "#ffffff",
#    "display": "standalone"
# }
# EOF
