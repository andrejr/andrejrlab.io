window.onload = function () {
  adjustPixelArtSize();
  window.onresize = adjustPixelArtSize;
};

function adjustPixelArtSize() {
  var imgElements = document.querySelectorAll(".pixel-art");

  imgElements.forEach(function (imgElement) {
    var parentElementWidth = imgElement.parentNode.offsetWidth;
    var originalImgWidth = imgElement.naturalWidth;
    var multiplier = Math.max(
      Math.floor(parentElementWidth / originalImgWidth),
      1
    );

    // Set the image width to the max multiple that can fit inside the parent
    imgElement.style.width = multiplier * originalImgWidth + "px";
    imgElement.style.height = "auto";
  });
}
